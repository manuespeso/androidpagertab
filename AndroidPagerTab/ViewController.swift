
import UIKit

class ViewController: UIViewController {
    
    lazy var viewPager: ViewPager = {
        let view1 = EmptyClass()
        view1.view.backgroundColor = .red
        let view2 = EmptyClass()
        view2.view.backgroundColor = .blue
        let view3 = EmptyClass()
        view3.view.backgroundColor = .orange
        
        let viewPager = ViewPager(
            tabSizeConfiguration: .fit(height: 44),
            vcs: [view1, view2, view3]
        )
        viewPager.translatesAutoresizingMaskIntoConstraints = false
        
        return viewPager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .cyan
        view.addSubview(viewPager)
        //viewPager.pagedView.enableScroll(enable: false)
        
        NSLayoutConstraint.activate([
            viewPager.widthAnchor.constraint(equalTo: view.widthAnchor),
            viewPager.heightAnchor.constraint(equalTo: view.heightAnchor),
            viewPager.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            viewPager.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        ])
    }
}

class EmptyClass: UIViewController, GibobsTabProperties {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func configureTab() -> GibobsTabModel {
        GibobsTabModel(title: "pepe", displayBadge: true)
    }
}
