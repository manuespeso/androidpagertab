
import UIKit

protocol GibobsPagedViewDelegate: AnyObject {
    func didMoveToPage(index: Int)
}

final class GibobsPagedView: UIView {
    
    // MARK: - Properties
    
    weak var delegate: GibobsPagedViewDelegate?
    
    private var pages: [UIViewController] = [] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        let collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints =  false
        collectionView.bounces = false
        
        collectionView.register(GibobsPageCollectionViewCell.self,
                                forCellWithReuseIdentifier: "gibobsPageCollectionViewCell")
        
        return collectionView
    }()
    
    // MARK: - Initialization
    
    init(pages: [UIViewController]) {
        super.init(frame: .zero)
        
        self.pages = pages
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    
    /// Move to specific page when user select any tab
    func moveToPage(at index: Int) {
        let indexPath = IndexPath(item: index, section: 0)
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    /// Enable or disable scroll from *ViewPager.pagedView.enableScroll(enable:)*
    func enableScroll(enable: Bool) {
        collectionView.isScrollEnabled = enable
    }
    
    private func setPagesProperty(pages: [UIViewController]) {
        //required this stuff for force call didSet method
        self.pages = pages
    }
    
    private func setupView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(collectionView)
        
        collectionView.backgroundColor = .brown
        
        NSLayoutConstraint.activate([
            collectionView.widthAnchor.constraint(equalTo: self.widthAnchor),
            collectionView.heightAnchor.constraint(equalTo: self.heightAnchor),
            collectionView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            collectionView.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
    }
}

// MARK: CollectionView Delegates

extension GibobsPagedView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let page = pages[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gibobsPageCollectionViewCell",
                                                      for: indexPath)
        
        if let cell = cell as? GibobsPageCollectionViewCell {
            cell.viewController = page
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: self.collectionView.frame.width, height: self.collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(self.collectionView.contentOffset.x / self.collectionView.frame.size.width)
        self.delegate?.didMoveToPage(index: page)
    }
}
