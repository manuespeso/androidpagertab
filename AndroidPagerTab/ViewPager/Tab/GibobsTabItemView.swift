
import UIKit

protocol GibobsTabItemProtocol: UIView {
    func onSelected()
    func onNotSelected()
}

class GibobsTabItemView: UIView {
    
    private let model: GibobsTabModel
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 17, weight: .semibold)
        label.textColor = .black
        label.text = model.title
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var badgeView: UIView = {
        let view = UIView()
        view.backgroundColor = .red
        view.isHidden = !model.displayBadge
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var borderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0.008, green: 0.765, blue: 0.804, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 1
        
        return view
    }()
    
    init(model: GibobsTabModel) {
        self.model = model
        
        super.init(frame: .zero)
        
        self.setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        badgeView.layer.cornerRadius = badgeView.frame.size.width/2
        badgeView.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI Setup
    
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        
        addSubview(titleLabel)
        addSubview(badgeView)
        
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            badgeView.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            badgeView.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 8),
            badgeView.widthAnchor.constraint(equalToConstant: 6),
            badgeView.heightAnchor.constraint(equalToConstant: 6)
        ])
    }
}


// MARK: Tab Delegate

extension GibobsTabItemView: GibobsTabItemProtocol {
    
    func onSelected() {
        titleLabel.textColor = UIColor(red: 0.008, green: 0.765, blue: 0.804, alpha: 1)
        
        if borderView.superview == nil {
            self.addSubview(borderView)
            
            NSLayoutConstraint.activate([
                borderView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
                borderView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
                borderView.heightAnchor.constraint(equalToConstant: 3),
                borderView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ])
        }
    }
    
    func onNotSelected() {
        titleLabel.textColor = UIColor(red: 0.184, green: 0.282, blue: 0.345, alpha: 1)
        borderView.removeFromSuperview()
    }
}
