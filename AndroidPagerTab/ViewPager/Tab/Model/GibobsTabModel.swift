
struct GibobsTabModel {
    let title: String
    let displayBadge: Bool
    
    init(title: String, displayBadge: Bool = false) {
        self.title = title
        self.displayBadge = displayBadge
    }
}
