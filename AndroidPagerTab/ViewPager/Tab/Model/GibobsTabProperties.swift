
protocol GibobsTabProperties {
    func configureTab() -> GibobsTabModel
}
