
import UIKit

final class ViewPager: UIView {
    
    private let sizeConfiguration: GibobsTabbedView.SizeConfiguration
    private let vcs: [UIViewController]
    
    lazy var pagedView: GibobsPagedView = {
        let pagedView = GibobsPagedView(pages: vcs)
        
        return pagedView
    }()
    
    lazy var tabbedView: GibobsTabbedView = {
        vcs.forEach { if !($0 is GibobsTabProperties) {
            fatalError("Every view controller provided must conform to GibobsTabProperties")
        }}
        let tabsConfiguration = vcs.compactMap { $0 as? GibobsTabProperties }
        
        let tabbedView = GibobsTabbedView(
            sizeConfiguration: sizeConfiguration,
            tabsConfiguration: tabsConfiguration
        )
        return tabbedView
    }()
    
    // MARK: - Initialization
    
    init(tabSizeConfiguration: GibobsTabbedView.SizeConfiguration, vcs: [UIViewController]) {
        self.sizeConfiguration = tabSizeConfiguration
        self.vcs = vcs
        
        super.init(frame: .zero)
        
        setupView()
        
        tabbedView.delegate =  self
        pagedView.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - UI Setup
    
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(tabbedView)
        addSubview(pagedView)
        
        NSLayoutConstraint.activate([
            tabbedView.leftAnchor.constraint(equalTo: self.leftAnchor),
            tabbedView.topAnchor.constraint(equalTo: self.topAnchor),
            tabbedView.rightAnchor.constraint(equalTo: self.rightAnchor),
            tabbedView.heightAnchor.constraint(equalToConstant: sizeConfiguration.height),
            pagedView.leftAnchor.constraint(equalTo: self.leftAnchor),
            pagedView.topAnchor.constraint(equalTo: self.tabbedView.bottomAnchor),
            pagedView.rightAnchor.constraint(equalTo: self.rightAnchor),
            pagedView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
}

extension ViewPager: GibobsTabbedViewDelegate, GibobsPagedViewDelegate {
    
    func didMoveToTab(at index: Int) {
        self.pagedView.moveToPage(at: index)
    }
    
    func didMoveToPage(index: Int) {
        self.tabbedView.moveToTab(at: index)
    }
}
